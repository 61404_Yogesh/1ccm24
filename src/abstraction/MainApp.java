package abstraction;
import java.util.Scanner;
public class MainApp {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("select account type");
        System.out.println("1:saving\t\t2:loan");
        int choice=sc.nextInt();
        System.out.println("enter account opening balance");
        double balance=sc.nextDouble();
        AccountFactory factory=new AccountFactory();
        Account accRef=factory.createAccount(choice,balance);
        System.out.println("===============================");
        boolean status=true;
        while(status){
            System.out.println("select mode of transaction");
            System.out.println("1: deposit");
            System.out.println("2: withdraw");
            System.out.println("3: check balance");
            System.out.println("4: exit");
            int mode=sc.nextInt();
            switch(mode){
                case 1:
                    System.out.println("enter amount");
                    double depositAmt=sc.nextDouble();
                    accRef.deposit(depositAmt);
                    break;
                case 2:
                    System.out.println("enter withdraw amount");
                    double withdrawAmt= sc.nextDouble();
                    accRef.withdraw(withdrawAmt);
                    break;
                case 3:
                    accRef.checkBalance();
                    break;
                case 4:
                    status=false;
            }
        }
    }
}
