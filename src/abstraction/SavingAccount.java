package abstraction;

public class SavingAccount implements Account{
    double accountBalance=0.0;
    public SavingAccount(double accountBalance){
        this.accountBalance=accountBalance;
        System.out.println("saving account created");
    }
    @Override
    public void deposit(double amt) {
        accountBalance+=amt;
        System.out.println(amt+"rs credited to your account");
    }
    public void withdraw(double amt){
        if(amt<=accountBalance){
            accountBalance-=amt;
            System.out.println(amt+"rs withdrawn from your account");
        }else{
            System.out.println("insufficient balance");
        }
    }
    @Override
    public void checkBalance(){
        System.out.println("account balance is"+accountBalance+"rs");
    }
}
