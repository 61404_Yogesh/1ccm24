package abstraction;

public class AccountFactory {
    Account createAccount(int choice,double Amt) {
        Account a1 = null;
        if (choice == 1) {
            a1 = new SavingAccount(Amt);
        } else if (choice == 2) {
            a1 = new LoanAccount(Amt);
        }
        return a1;
    }
}