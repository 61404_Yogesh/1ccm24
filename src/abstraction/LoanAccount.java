package abstraction;

public class LoanAccount implements Account{
    double LoanAmount=0.0;
    public LoanAccount(double LoanAmount){
        this.LoanAmount=LoanAmount;
        System.out.println("loan account created");
    }
    @Override
    public void deposit(double amt) {
        LoanAmount-=amt;
        System.out.println(amt+"rs debited to your account");
    }
    @Override
    public void withdraw(double amt){
        LoanAmount+=amt;
        System.out.println(amt+"rs withdrawn from your account");
        System.out.println("insufficient balance");
        }
    @Override
    public void checkBalance(){
        System.out.println("account balance is"+LoanAmount+"rs");
    }
}
